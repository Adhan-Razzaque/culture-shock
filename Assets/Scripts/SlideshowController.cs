using System.Collections.Generic;
using UnityEngine;

public class SlideshowController : MonoBehaviour
{
    public List<GameObject> slides;

    private int _currentSlide;

    private void Awake()
    {
        foreach (var slide in slides)
        {
            slide.SetActive(false);
        }

        _currentSlide = -1;
    }

    public void ShowSlide(int index)
    {
        if (index < 0 || index >= slides.Count)
        {
            Debug.LogError($"Slide index {index} is out of range of slides.");
        }

        if (_currentSlide >= 0)
        {
            slides[_currentSlide].SetActive(false);
        }
        
        slides[index].SetActive(true);

        _currentSlide = index;
    }
}