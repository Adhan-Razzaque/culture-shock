using System.Collections;
using System.Collections.Generic;
using Managers;
using TMPro;
using UnityEngine;

public class FinalResults : MonoBehaviour
{
    public TMP_Text resultsText;
    
    // Start is called before the first frame update
    void Start()
    {
        resultsText.text = GameManager.Instance.GetFinalResults();
    }
}
