using System;
using Data;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class DoItYourselfController : MonoBehaviour
{
    public TMP_InputField inputText;
    public TMP_InputField outputText;
    public bool GotResponse { get; private set; }
    
    public UnityEvent responseReceived;

    private PromptResponse _response;
    private bool _pendingResponse;

    public void Reset()
    {
        inputText.text = "";
        outputText.text = "";
        _response = new PromptResponse();
        _pendingResponse = false;
        GotResponse = false;
    }

    private void OnResponseReceived(string response)
    {
        Debug.Log($"Received response: {response}");
        outputText.text = response;
        _response.Output = response;
        _pendingResponse = false;
        responseReceived.Invoke();
        GotResponse = true;
    }

    public void OnSubmit()
    {
        if (_pendingResponse) return;

        _pendingResponse = true;
        _response.Input = inputText.text;
        OpenAIManager.Instance.Execute(inputText.text, OnResponseReceived);
    }

    public PromptResponse GetResponse()
    {
        return _response;
    }
}