﻿namespace Data
{
    public struct PromptResponse
    {
        public string Input;
        public string Output;
    }
}