﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Managers
{
    public class GameManager : Singleton<GameManager>
    {
        public SlideshowController slideController;
        public DoItYourselfController diyController;
        public TMP_Text promptText;
        public Button nextButton;
        public Button prevButton;
        public Button doneButton;

        [SerializeField] private int numSlides;
        [SerializeField] private List<string> prompts;
        private PromptResponse[] _responses;

        private int _currentSlide;
        // private int _currentMax;

        private void Start()
        {
            if (prompts.Count != numSlides)
            {
                Debug.LogError("Prompts count does not match number of slides");
            }

            _responses = new PromptResponse[numSlides];

            if (nextButton)
            {
                nextButton.onClick.AddListener(NextSlide);
            }

            if (prevButton)
            {
                prevButton.onClick.AddListener(PrevSlide);
            }

            if (doneButton)
            {
                doneButton.gameObject.SetActive(false);
            }

            _currentSlide = 0;
            slideController.ShowSlide(_currentSlide);
            promptText.text = prompts[_currentSlide];
            diyController.responseReceived.AddListener(ReceivedResponse);
        }

        public void ReceivedResponse()
        {
            _responses[_currentSlide] = diyController.GetResponse();
        }

        public void NextSlide()
        {
            if (_currentSlide + 1 == numSlides)
            {
                return;
            }

            if (diyController.GotResponse)
            {
                _responses[_currentSlide] = diyController.GetResponse();
                diyController.Reset();
            }

            ++_currentSlide;
            if (slideController)
            {
                slideController.ShowSlide(_currentSlide);
            }

            promptText.text = prompts[_currentSlide];

            if (_currentSlide + 1 == numSlides)
            {
                nextButton.interactable = false;
                doneButton.gameObject.SetActive(true);
            }

            prevButton.interactable = true;
        }

        public void PrevSlide()
        {
            if (_currentSlide == 0)
            {
                return;
            }

            if (diyController.GotResponse)
            {
                _responses[_currentSlide] = diyController.GetResponse();
                diyController.Reset();
            }

            --_currentSlide;
            if (slideController)
            {
                slideController.ShowSlide(_currentSlide);
            }
            
            promptText.text = prompts[_currentSlide];
            
            if (_currentSlide == 0)
            {
                prevButton.interactable = false;
            }

            nextButton.interactable = true;
            doneButton.gameObject.SetActive(false);
        }

        public string GetFinalResults()
        {
            var sb = new StringBuilder();

            for (var i = 0; i < prompts.Count; ++i)
            {
                var prompt = prompts[i];
                var response = _responses[i];
                sb.Append($"Prompt: {prompt}\n");
                sb.Append($"Input: {response.Input}\n");
                sb.Append($"Output: {response.Output}\n");
                sb.Append("\n");
            }

            return sb.ToString();
        }

        public void HandleDone()
        {
            SceneManager.LoadScene(1);
        }
    }
}